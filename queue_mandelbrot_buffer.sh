#!/bin/bash

#$ -cwd
#$ -pe orte 20
#$ -N PA2_Mandelbrot
#$ -m bes
#$ -M tjbk123@gmail.com

export LD_LIBRARY_PATH=/usr/lib64/openmpi-1.5.3/lib:$LD_LIBRARY_PATH
export PATH=/usr/lib64/openmpi-1.5.3/bin:$PATH


nodes=20
for test_num in $(seq 0 5)
do
	for n in $(seq 500 500 10000)
	do
		time=$(/usr/lib64/openmpi-1.5.3/bin/mpirun -np 1 bash -c "echo 1,$n,$(($n*$n)),$(./bin/mandelbrot_serial --width $n --height $n out.png) >> times.txt")

		for bufsize in $(seq -f %.00f $(echo "$n*$n/$nodes/100"|bc) $(echo "$n*$n/$nodes/100"|bc) $(echo "$n*$n/$nodes/2"|bc)) 
		do
			echo $nodes,$n,$bufsize,$(/usr/lib64/openmpi-1.5.3/bin/mpirun -np 20 ./bin/mandelbrot_parallel --width $n --height $n --buffer $bufsize out.png) >> times.txt
		done
	done
done


cmake_minimum_required (VERSION 2.6)
project (Mandelbrot)

SET(CMAKE_C_COMPILER /usr/lib64/openmpi-1.5.3/bin/mpicc)
SET(CMAKE_CXX_COMPILER /usr/lib64/openmpi-1.5.3/bin/mpic++)

find_package(Boost 1.45 COMPONENTS thread program_options REQUIRED)
find_package(PNG REQUIRED)

link_directories ( ${Boost_LIBRARY_DIRS} )
include_directories ( ${Boost_INCLUDE_DIRS} ${PNG_INCLUDE_DIR} )

add_executable(mandelbrot_serial mandelbrot_serial.cpp)
target_link_libraries(mandelbrot_serial ${Boost_LIBRARIES} ${PNG_LIBRARY})

add_executable(mandelbrot_parallel mandelbrot_parallel.cpp)
target_link_libraries(mandelbrot_parallel ${Boost_LIBRARIES} ${PNG_LIBRARY})


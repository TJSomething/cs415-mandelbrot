#include <boost/gil/image.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include <complex>
#include <vector>
#include <boost/function.hpp>
#include <boost/program_options.hpp>
#include <string>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/array.hpp>
#include <sstream>
#include <set>
#include <mpi.h>
#include <queue>
#include <time.h>

using namespace std;
using namespace boost;
using namespace boost::gil;
using namespace boost::program_options;

typedef point2<ptrdiff_t> point_t;

const int WORK_REQUEST = 0;
const int WORK_RESULT = 1;
const int WORK = 2;
const int DIE = 3;

unsigned int mandelbrotIterations(complex<double> p, int iterMax)
{
    complex<double> z(0,0);
    for(int i = 1; i <= iterMax; i++)
    {
        z = z*z + p;
        if (norm(z) > 4)
            return i;
    }
    return 0;
}

template <typename number>
double scale(number t, number t0, number t1, double u0, double u1)
{
    return ((t-t0)/(double)(t1-t0))*(u1-u0)+u0;
}

template <typename color_t>
struct workResult
{
    vector<color_t> xs;
    point2<ptrdiff_t> startPoint;
    bool operator<(const workResult<color_t>&) const;
    bool operator>(const workResult<color_t>&) const;
};

template <typename color_t>
bool workResult<color_t>::operator<(const workResult<color_t>& rhs) const
{
    return (startPoint.y < rhs.startPoint.y) ||
        (startPoint.y == rhs.startPoint.y &&
         startPoint.x < rhs.startPoint.x);
}

template <typename color_t>
bool workResult<color_t>::operator>(const workResult<color_t>& rhs) const
{
    return (startPoint.y > rhs.startPoint.y) ||
        (startPoint.y == rhs.startPoint.y &&
         startPoint.x > rhs.startPoint.x);
}

rgb8_pixel_t mandelbrotColors(unsigned int iters, unsigned int period)
{
    if (iters == 0)
        return rgb8_pixel_t(0,0,0);
    else
    {
        unsigned int h = (unsigned int)(iters*1530.0/period) % 1530;
        unsigned int section = h/255;
        unsigned char m = h % 255;
        unsigned char r = 0, g = 0, b = 0;
        switch (section)
        {
            case 0:
                r = 255;
                g = m;
                break;
            case 1:
                r = 255 - m;
                g = 255;
                break;
            case 2:
                g = 255;
                b = m;
                break;
            case 3:
                g = 255 - m;
                b = 255;
                break;
            case 4:
                b = 255;
                r = m;
                break;
            case 5:
                b = 255 - m;
                r = 255;
                break;
        }
        return rgb8_pixel_t(r,g,b);
    }    
}
template <typename color_t>
class mandelbrotImage
{
    public:

        mandelbrotImage(const point_t& sz,
                complex<double> ul,
                complex<double> lr,
                unsigned int newMaxIter,
                function<color_t (unsigned int)> conFunc) :
            dimensions(sz),
            upperLeft(ul),
            lowerRight(lr),
            maxIter(newMaxIter),
            convertToColor(conFunc) {};

        color_t getPoint(const point_t& coord)
        {
            complex<double> p(
                    scale(coord.x+0.5, 0., double(dimensions.x),
                        real(upperLeft), real(lowerRight)),
                    scale(coord.y+0.5, 0., double(dimensions.y),
                        imag(upperLeft), imag(lowerRight)));
            return convertToColor(mandelbrotIterations(p, maxIter));
        }

        workResult<color_t> getPoints(const point_t& startPoint, unsigned int sz)
        {
            workResult<color_t> points;
            points.startPoint = startPoint;
            point_t currentPoint = startPoint;
            // Allocate space
            points.xs.reserve(sz);
            
            // For all the points we are calculating
            while (sz > 0)
            {
                // Store the point
                points.xs.push_back(getPoint(currentPoint));
                // Go to the next point
                currentPoint.x = (currentPoint.x + 1) % dimensions.x;
                if (currentPoint.x == 0)
                    currentPoint.y++;
                // Decrement the number of points remaining
                sz--;
            }
            return points;
        }

    private:
        complex<double> upperLeft;
        complex<double> lowerRight;
        point_t dimensions;
        unsigned int maxIter;
        function<color_t (unsigned int iter)> convertToColor;
};

struct work
{
    point_t startPoint;
    ptrdiff_t length;
    bool operator<(const work&) const;
};

bool work::operator<(const work& rhs) const
{
    return (startPoint.y < rhs.startPoint.y) ||
        (startPoint.y == rhs.startPoint.y &&
         startPoint.x < rhs.startPoint.x);
}

class master
{
    public:
        master(string, unsigned int, unsigned int);
        rgb8_view_t run();
    private:
        work pop(ptrdiff_t length);
        void mergeResults(const workResult<rgb8_pixel_t>*);
        void transaction(int node);
        point_t toPoint(unsigned int);
        unsigned int fromPoint(point_t);
        unsigned int w;
        unsigned int h;
        unsigned int working;
        unsigned int finished;
        priority_queue<workResult<rgb8_pixel_t>,
            vector<workResult<rgb8_pixel_t> >,
            greater<workResult<rgb8_pixel_t> > > unmerged;
        vector<rgb8_pixel_t> merged;
        string filename;
        mutex mergeMutex;
};

master::master(string fName, unsigned int width, unsigned int height)
{
    w = width;
    h = height;
    finished = 0;
    working = 0;
    merged.resize(w*h);
    filename = fName;
}

point_t master::toPoint(unsigned int pos)
{
    return point_t(pos % w, pos / w);
}

unsigned int master::fromPoint(point_t pt)
{
    return pt.y*w+pt.x;
}

work master::pop(ptrdiff_t length)
{
    work newWork;
    if (working < w*h)
    {
        newWork.startPoint = toPoint(working);
        /* cout << "Master: The next available work is " << working;
        cout.flush() */;
        newWork.length = length;
        working += length;
        /* cout << " to " << working << endl;
            cout.flush() */;
        if (working > w*h)
        {
            newWork.length -= working - w*h;
            working = w*h;
        }
        return newWork;
    }
    else if (finished < w*h)
    {
        working = finished;
        return pop(length);
    }
    else
    {
        newWork.length = 0;
        return newWork;
    }
}

void master::mergeResults(const workResult<rgb8_pixel_t>* x)
{
    workResult<rgb8_pixel_t> tmpResult;
    unsigned int next, i;
    mutex::scoped_lock l(mergeMutex);
    // Put the results into the unmerged list
    unmerged.push(*x);
    delete x;
    /* cout << "Master: Pushing result from " << fromPoint(x.startPoint)<<
        " to " << fromPoint(x.startPoint) + x.xs.size() << endl;
            cout.flush(); */
    // Delete results that have already been seen
    while ((not unmerged.empty()) &&
            (fromPoint(unmerged.top().startPoint) +
            unmerged.top().xs.size() <= finished))
    {
        /* cout << "Master: Skipping " << fromPoint(unmerged.top().startPoint) <<
            " to " << 
            (fromPoint(unmerged.top().startPoint) +
             unmerged.top().xs.size()) << endl;
            cout.flush(); */
        unmerged.pop();
    }
    // Until the first, non-contiguous, unmerged result
    while((not unmerged.empty()) &&
          (fromPoint(unmerged.top().startPoint) == finished) ||
          (fromPoint(unmerged.top().startPoint) < finished &&
           fromPoint(unmerged.top().startPoint) +
           unmerged.top().xs.size() > finished))
    {
        // Pull out the first mergable result
        tmpResult = unmerged.top();
        unmerged.pop();
        finished = fromPoint(tmpResult.startPoint);
        /* cout << "Master: Copying result from "<< finished << " to " << finished + tmpResult.xs.size() << "..." << endl;
        cout.flush(); */
        // Merge the result
        memcpy(&(merged[finished]),
                &(tmpResult.xs[0]),
                sizeof(rgb8_pixel_t)*tmpResult.xs.size());
        /* cout << "Master: Incrementing counter..." << endl;
        cout.flush(); */
        finished += tmpResult.xs.size();
    }
}

rgb8_view_t master::run()
{
    unsigned int end = w*h, x, y, length;
    MPI_Status status;
    char *buffer = NULL, *buffer2 = NULL;
    work newWork;
    workResult<rgb8_pixel_t> *tmpResult=NULL;
    int taskCount, bufsize, flag;
    ostringstream sendStream;
    thread_group mergeJobs;

    // Find number of tasks running
    MPI_Comm_size(MPI_COMM_WORLD, &taskCount);

    // Until we've calculated all of the pixels
    while (finished < end)
    {
        // Get the length of the next message
        /* cout << "Master: Waiting for the next message..." << finished << endl;
        cout.flush(); */
        MPI_Probe(
                MPI_ANY_SOURCE,
                MPI_ANY_TAG,
                MPI_COMM_WORLD,
                &status);
        MPI_Get_count(&status, MPI_CHAR, &bufsize);
        // Allocate space for the message
        /* cout << "Master: Reading the next message..." << endl;
        cout.flush(); */
        buffer = new char[bufsize];
        MPI_Recv(buffer,
                bufsize,
                MPI_BYTE,
                status.MPI_SOURCE,
                status.MPI_TAG,
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE);
        istringstream iss(buffer);
        // If it's a work request
        if (status.MPI_TAG == WORK_REQUEST)
        {
            // Deserialize request
            /* cout << "Master: Deserializing request... " << buffer << endl;
            cout.flush() ; */
            iss >> length;
            // Get the next available work
            /* cout << "Master: Getting next available work..." <<endl;
            cout.flush(); */
            newWork = pop(length);
            // Serialize it 
            sendStream << newWork.startPoint.x << " " <<
                newWork.startPoint.y << " " <<
                newWork.length << " ";
            buffer2 = new char[sendStream.str().size()+1];
            strcpy(buffer2, sendStream.str().c_str());
            // Send it
            MPI_Send(buffer2,
                    sendStream.str().size()+1,
                    MPI_CHAR,
                    status.MPI_SOURCE,
                    WORK,
                    MPI_COMM_WORLD);
            delete buffer2;
            sendStream.str("");
        }
        // If it's a work result
        else if (status.MPI_TAG == WORK_RESULT)
        {
            /* cout << "Master: Deserializing result..." << endl;
            cout.flush(); */
            tmpResult = new workResult<rgb8_pixel_t>();
            // Deserialize the result
            iss >> tmpResult->startPoint.x >>
                tmpResult->startPoint.y;
            iss.ignore(); // Skip space
            // The size of the result in bytes
            length = bufsize - iss.tellg();
            tmpResult->xs.resize(length/3);
            // Copy the result over
            memcpy(&(tmpResult->xs[0]), buffer + iss.tellg(), length);
            /* cout << "Master: Merging result from slave " <<
                status.MPI_SOURCE <<
                " starting at (" <<
                tmpResult.startPoint.x << "," <<
                tmpResult.startPoint.y << ") of length " << length/3 << endl;
            cout.flush() ; */
            // Merge it into the image
            mergeJobs.create_thread(boost::bind(&master::mergeResults, this, tmpResult));
        }
        delete buffer;
    }
    // Clean up slaves
    for (int i = 1; i < taskCount; i++)
    {
        /* cout << "Master: Telling slave " << i << "/" << taskCount<< " to die..." << endl;
        cout.flush(); */
        MPI_Send(0,0,MPI_INT, i, DIE, MPI_COMM_WORLD);
    }
    // Wait for merge jobs to finish
    mergeJobs.join_all();

    return interleaved_view(w, h, &merged[0], w*sizeof(rgb8_pixel_t));
}

class slave
{
    public:
        slave(const mandelbrotImage<rgb8_pixel_t>&, unsigned int);
        int run();
        int node;
    private:
        mandelbrotImage<rgb8_pixel_t> imageFn;
        unsigned int _requestSize;
};

slave::slave(const mandelbrotImage<rgb8_pixel_t>& imageParams,
        unsigned int requestSize) :
    imageFn(imageParams), _requestSize(requestSize) {}

int slave::run()
{
    char *packetBuffer = NULL;
    ostringstream sendPacket;
    istringstream recvPacket;
    MPI_Status status;
    int bufsize, flag;
    unsigned int x, y, size;
    workResult<rgb8_pixel_t> resultToSend;
    MPI_Request req;
    // Until the program is stopped
    while (1)
    {
        // Request work
        /* cout << "Slave " << node << ": Assembling work request..." << endl;
        cout.flush() ; */
        sendPacket.str("");
        sendPacket << _requestSize;
        packetBuffer = new char[sendPacket.str().size()+1];
        strcpy(packetBuffer, sendPacket.str().c_str());
        /* cout << "Slave " << node << ": Requesting work: " << packetBuffer << endl;
        cout.flush() ; */
        MPI_Send(packetBuffer,
                sendPacket.str().length()+1,
                MPI_CHAR,
                0,
                WORK_REQUEST,
                MPI_COMM_WORLD);
        delete packetBuffer;
        /* cout << "Slave " << node << ": waiting for reply..." << endl;
        cout.flush() ; */
        // Wait for a message and check message type and size
        MPI_Probe(0,
                MPI_ANY_TAG,
                MPI_COMM_WORLD,
                &status);
        // If ordered to work
        if (status.MPI_TAG == WORK)
        {
            // Allocate space for the message
            /* cout << "Slave " << node << ": Allocating message space..." << endl;
            cout.flush() ; */
            MPI_Get_count(&status, MPI_CHAR, &bufsize);
            packetBuffer = new char[bufsize];
            // Get work message
            /* cout << "Slave " << node << ": Getting message..." << endl;
            cout.flush() ; */
            MPI_Recv(packetBuffer,
                    bufsize,
                    MPI_CHAR,
                    0,
                    WORK,
                    MPI_COMM_WORLD,
                    MPI_STATUS_IGNORE);
            // Deserialize work message
            /* cout << "Slave " << node << ": Deserializing message..." << packetBuffer << endl;
            cout.flush() ; */
            recvPacket.str(packetBuffer);

            /* cout << "Slave " << node << ": Reading stream..." << endl;
            cout.flush() ; */
            recvPacket >> x >> y >> size;
            /* cout << "Slave " << node << ": Deallocating message..." << endl;
            cout.flush() ; */
            delete packetBuffer;
            // Do work
            /* cout << "Slave " << node << ": Doing work starting at (" << x << ", " <<  y << ") of length " << size << endl;
            cout.flush() ; */
            resultToSend = imageFn.getPoints(point_t(x,y), size);
            // Serialize results
            /* cout << "Slave " << node << ": Assembling results..." << endl;
            cout.flush() ; */
            sendPacket.str("");
            sendPacket << x << " " << y << " ";
            /* cout << "Slave  " << node << ": Result header \"" << sendPacket.str() << '"' << endl;
            cout.flush() ; */
            sendPacket.write((char*)(&resultToSend.xs[0]),
                    resultToSend.xs.size()*3);
            // Send results
            packetBuffer = new char[sendPacket.str().size()];
            /* cout << "Slave " << node << ": Copying results into buffer..." << endl;
            cout.flush() ; */
            memcpy(packetBuffer, sendPacket.str().data(), sendPacket.str().size());
            /* cout << "Slave " << node << ": Sending results..." << endl;
            cout.flush(); */
            MPI_Send(packetBuffer,
                    sendPacket.str().length(),
                    MPI_BYTE,
                    0,
                    WORK_RESULT,
                    MPI_COMM_WORLD);
            /* cout << "Slave " << node << ": Deallocating buffer..." << endl;
            cout.flush(); */
            delete packetBuffer;
        }
        else
        {
            /* cout << "Slave " << node << ": Dying..." << endl;
            cout.flush() ; */
            // If ordered to die, die
            return 0;
        }
    }
    // We should not reach here
    return 1;
}

int main(int argc, char **argv)
{
    unsigned int w, h, iters, colors, bufsize, maxNodes;
    double t, r, b, l, totalTime;
    int node;
    string file;
    options_description desc("Arguments");
    positional_options_description p;
    options_description hidden("Hidden");
    options_description allArgs("Any args");
    variables_map vm;
    master* mJob;
    slave* sJob;
    timespec startTime, endTime;
    rgb8_view_t image;

    MPI_Init(&argc, &argv);

    // Parse arguments
    desc.add_options()
        ("help,h", "print help")
        ("width",
         value<unsigned int>(&w)->default_value(200),
         "output image width")
        ("height",
         value <unsigned int>(&h)->default_value(200),
         "output image height")
        ("right,r",
         value<double>(&r)->default_value(2.0),
         "maximum real value")
        ("left,l",
         value<double>(&l)->default_value(-2.0),
         "minimum real value")
        ("top,t",
         value<double>(&t)->default_value(2.0),
         "maximum imaginary value")
        ("bottom,b",
         value<double>(&b)->default_value(-2.0),
         "minimum imaginary value")
        ("iterations,i",
         value<unsigned int>(&iters)->default_value(256),
         "maximum iterations")
        ("colors,c",
         value<unsigned int>(&colors)->default_value(16),
         "number of colors to use")
        ("buffer",
         value<unsigned int>(&bufsize)->default_value(1000),
         "number of pixels to send at once")
        ;
    hidden.add_options()
        ("output,o",
         value<string>(&file),
         "output file")
        ;
    allArgs.add(desc).add(hidden);

    p.add("output", -1);

    store(
        command_line_parser(argc,argv)
            .options(allArgs)
            .allow_unregistered()
            .positional(p)
            .run(),
        vm);
    notify(vm);

    // If help is an argument or output is missing return help and exit
    if (vm.count("help") or not vm.count("output"))
    {
        cout << "Usage: " << argv[0] << " [<arguments>] <output file>" << endl;
        cout << desc << endl;
        return 1;
    }

    // Get the node number
    MPI_Comm_rank(MPI_COMM_WORLD, &node);

    // Run the appropriate stuff
    if (node == 0)
    {
        mJob = new master(file, w, h);
        clock_gettime(CLOCK_MONOTONIC, &startTime);
        image = mJob->run();
        clock_gettime(CLOCK_MONOTONIC, &endTime);
        totalTime = endTime.tv_sec - startTime.tv_sec;
        totalTime += (endTime.tv_nsec - startTime.tv_nsec)*1e-9;
        cout << totalTime << endl;
        // Write the image
        png_write_view(file, image);
        // Kill off any stragglers
        MPI_Abort(MPI_COMM_WORLD, 0);
    }
    else
    {
        sJob = new slave(
                mandelbrotImage<rgb8_pixel_t>(
                    point2<ptrdiff_t>(w,h),
                    complex<double>(l,t),
                    complex<double>(r,b),
                    iters,
                    boost::bind<rgb8_pixel_t>(mandelbrotColors, _1, colors)),
                bufsize);
        sJob->node = node;
        sJob->run();
    }

    // Kill
    MPI_Finalize();

    return 0;
}

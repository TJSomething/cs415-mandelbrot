/*
 * =====================================================================================
 *
 *       Filename:  mandelbrot_serial.cpp
 *
 *    Description:  This is a simple implementation of a serial Mandelbrot algorithm
 *                  based on an example from Boost's source code.
 *
 *        Version:  1.0
 *        Created:  02/15/2012 12:14:53 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author: Thomas Kelly (tjbk123@gmail.com) 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <boost/gil/image.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include <complex>
#include <vector>
#include <boost/function.hpp>
#include <boost/program_options.hpp>
#include <string>
#include <boost/bind.hpp>
#include <iostream>
#include <time.h>

using namespace std;
using namespace boost;
using namespace boost::gil;
using namespace boost::program_options;

unsigned int mandelbrotIterations(complex<double> p, int iterMax)
{
    complex<double> z(0,0);
    for(int i = 1; i <= iterMax; i++)
    {
        z = z*z + p;
        if (norm(z) > 4)
            return i;
    }
    return 0;
}

template <typename number>
double scale(number t, number t0, number t1, double u0, double u1)
{
    return ((t-t0)/(double)(t1-t0))*(u1-u0)+u0;
}

template <typename color_t>
struct workResult
{
    public:
        vector<color_t> xs;
        point2<ptrdiff_t> startPoint;
};

rgb8_pixel_t mandelbrotColors(unsigned int iters, unsigned int period)
{
    if (iters == 0)
        return rgb8_pixel_t(0,0,0);
    else
    {
        unsigned int h = (unsigned int)(iters*1530.0/period) % 1530;
        unsigned int section = h/255;
        unsigned char m = h % 255;
        unsigned char r = 0, g = 0, b = 0;
        switch (section)
        {
            case 0:
                r = 255;
                g = m;
                break;
            case 1:
                r = 255 - m;
                g = 255;
                break;
            case 2:
                g = 255;
                b = m;
                break;
            case 3:
                g = 255 - m;
                b = 255;
                break;
            case 4:
                b = 255;
                r = m;
                break;
            case 5:
                b = 255 - m;
                r = 255;
                break;
        }
        return rgb8_pixel_t(r,g,b);
    }    
}
template <typename color_t>
class mandelbrotImage
{
    public:
        typedef point2<ptrdiff_t> point_t;

        mandelbrotImage(const point_t& sz,
                complex<double> ul,
                complex<double> lr,
                unsigned int newMaxIter,
                function<color_t (unsigned int)> conFunc) :
            dimensions(sz),
            upperLeft(ul),
            lowerRight(lr),
            maxIter(newMaxIter),
            convertToColor(conFunc) {};

        color_t getPoint(const point_t& coord)
        {
            complex<double> p(
                    scale(coord.x+0.5, 0., double(dimensions.x),
                        real(upperLeft), real(lowerRight)),
                    scale(coord.y+0.5, 0., double(dimensions.y),
                        imag(upperLeft), imag(lowerRight)));
            return convertToColor(mandelbrotIterations(p, maxIter));
        }

        workResult<color_t> getPoints(const point_t& startPoint, unsigned int sz)
        {
            workResult<color_t> points;
            points.startPoint = startPoint;
            point_t currentPoint = startPoint;
            // Allocate space
            points.xs.reserve(sz);
            
            // For all the points we are calculating
            while (sz > 0)
            {
                // Store the point
                points.xs.push_back(getPoint(currentPoint));
                // Go to the next point
                currentPoint.x = (currentPoint.x + 1) % dimensions.x;
                if (currentPoint.x == 0)
                    currentPoint.y++;
                // Decrement the number of points remaining
                sz--;
            }
            return points;
        }

    private:
        complex<double> upperLeft;
        complex<double> lowerRight;
        point_t dimensions;
        unsigned int maxIter;
        function<color_t (unsigned int iter)> convertToColor;
};

int main(int argc, char **argv)
{
    unsigned int w, h, iters, colors;
    double t, r, b, l, totalTime;
    string file;
    timespec startTime, endTime;

    // Parse arguments
    options_description desc("Arguments");
    desc.add_options()
        ("help,h", "print help")
        ("width",
         value<unsigned int>(&w)->default_value(200),
         "output image width")
        ("height",
         value <unsigned int>(&h)->default_value(200),
         "output image height")
        ("right,r",
         value<double>(&r)->default_value(2.0),
         "maximum real value")
        ("left,l",
         value<double>(&l)->default_value(-2.0),
         "minimum real value")
        ("top,t",
         value<double>(&t)->default_value(2.0),
         "maximum imaginary value")
        ("bottom,b",
         value<double>(&b)->default_value(-2.0),
         "minimum imaginary value")
        ("iterations,i",
         value<unsigned int>(&iters)->default_value(256),
         "maximum iterations")
        ("colors,c",
         value<unsigned int>(&colors)->default_value(16),
         "number of colors to use")
        ;
    options_description hidden("Hidden");
    hidden.add_options()
        ("output,o",
         value<string>(&file),
         "output file")
        ;
    options_description allArgs("Any args");
    allArgs.add(desc).add(hidden);

    positional_options_description p;
    p.add("output", -1);

    variables_map vm;
    parsed_options parsed = command_line_parser(argc,argv)
        .options(allArgs)
        .allow_unregistered()
        .positional(p)
        .run();
    store(parsed, vm);
    notify(vm);

    // If help is an argument or output is missing return help and exit
    if (vm.count("help") or not vm.count("output"))
    {
        cout << "Usage: " << argv[0] << " [<arguments>] <output file>" << endl;
        cout << desc << endl;
        return 1;
    }
    clock_gettime(CLOCK_MONOTONIC, &startTime);
    mandelbrotImage<rgb8_pixel_t> img(
            point2<ptrdiff_t>(w,h),
            complex<double>(l,t),
            complex<double>(r,b),
            iters,
            boost::bind<rgb8_pixel_t>(mandelbrotColors, _1, colors));
    vector<rgb8_pixel_t> pixels = img.getPoints(
            point2<ptrdiff_t>(0,0),
            w*h).xs;
    rgb8_view_t image = interleaved_view(w, h, &pixels[0], w*sizeof(rgb8_pixel_t));
    clock_gettime(CLOCK_MONOTONIC, &endTime);
    totalTime = endTime.tv_sec - startTime.tv_sec;
    totalTime += (endTime.tv_nsec - startTime.tv_nsec)*1e-9;
    cout << totalTime << endl;
    png_write_view(file, image);

    return 0;
}

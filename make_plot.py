#!/usr/bin/python
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import csv
from matplotlib.mlab import griddata 
from matplotlib import ticker
import sys
from pprint import pprint
from fractions import gcd

# Get data
f = open(sys.argv[1], 'r')
reader = csv.reader(f)

col_names = ["Processors", "Size (pixels)", "Average buffers per node"]
cols = [0,1]
acc = {}
baseline = {}
for row in reader:
	if len(row) == 4:
		row[2] = float(row[2])*100*float(row[0])/float(row[1])/float(row[1])
	if acc.has_key((int(row[cols[0]]), int(row[cols[1]]))):
		acc[(int(row[cols[0]]), int(row[cols[1]]))].append(float(row[-1]))
	else:
		acc[(int(row[cols[0]]), int(row[cols[1]]))] = [float(row[-1])]
	if row[0] == '1':
		baseline[int(row[1])] = float(row[-1])

data = []
for xy, times in acc.iteritems():
	data.append((float(xy[0]), float(xy[1]), sum(times)/len(times)))
	
data.sort()
X, Y, Z = zip(*data)
#pprint(data)

# Calculate speedup and efficiency
speedups = [baseline[y]/z for x, y, z in data]
efficiency = [baseline[y]/z/x for x, y, z in data]

def plot_data(name, vals):
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	#X = np.arange(1, 51, 1)
	#Y = np.arange(400, 8000, 200)
	print "Calculating mesh coordinates..."
	xi, yi = np.meshgrid(np.arange(min(X), max(X), reduce(gcd,X)), np.arange(min(Y), max(Y), reduce(gcd,Y)))
	print "Interpolating data to mesh coordinates..."
	zi = griddata(X,Y,vals,xi,yi,interp='linear')
	print "Plotting data..."
	surf = ax.plot_surface(xi, yi, zi, rstride=1, cstride=1, cmap=cm.jet,
		linewidth=1, antialiased=True)
	ax.set_xlabel(col_names[cols[0]])
	def formatPixels(pixels, x):
		return str(int(pixels))
	yticklabels = ticker.FuncFormatter(formatPixels)
	ax.w_yaxis.set_major_formatter(yticklabels)
	ax.set_ylabel(col_names[cols[1]])
	ax.set_zlabel(name)
	plt.show()

plot_data("Time taken", Z)
plot_data("Speedup", speedups)
plot_data("Efficiency", efficiency)

#!/bin/bash

#$ -cwd
#$ -pe orte 5 
#$ -N PA2_Mandelbrot_Single
#$ -m bes

export LD_LIBRARY_PATH=/usr/lib64/openmpi-1.5.3/lib:$LD_LIBRARY_PATH
export PATH=/usr/lib64/openmpi-1.5.3/bin:$PATH

n=5000
nodes=5
bufsize=$(echo "$n*$n/16/$nodes"|bc)
/usr/lib64/openmpi-1.5.3/bin/mpirun -np $nodes ./bin/mandelbrot_parallel --width $n --height $n --buffer $bufsize out_test.png

